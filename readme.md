# Raspberry Pi Time Lapse

This code allows the Raspberry Pi to take a photo at set intervals - starting every day at Sunrise and stopping at Sunset.

## Installation

Download the repo to your home directory and install the following modules:

    sudo apt-get update
    sudo apt-get install python-pip
    sudo apt-get install python-picamera

And once `pip` is installed - install the forecastio module.

    sudo pip install python-forecastio

## Setup

The PiLapse needs a few files to get running.

### Config File

Sign up for an API key at [Dark Sky](https://developer.forecast.io/).

Create a file called `_config.cfg` with the following format:

    [forecastio]
    api_key = API KEY
    location_lat = 1.123
    location_lng = -1.23

    [pilapse]
    image_path = /home/pi/PiLapse/images
    interval = 60

The image path points to where you want the captured images to go. The interval is how many minutes to wait until taking the next picture.

### PiLapse file

In a similar vein, create a file called `_pilapse.cfg` which follows a similar format to the config file. Copy and paste the following into your file:

    [status]
    sunrise = 0
    sunset = 0
    date = 0
    last_updated = 0

    [photos]
    sunrise = False
    sunset = True
    last_taken = 0